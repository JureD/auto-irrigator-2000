#include <Wire.h>
#include <EEPROM.h>
#include "LiquidCrystal_I2C.h"
#include "RTClib.h"

#define CLEAR_EEPROM 0
#define ENCODER_CLK_PIN 3
#define ENCODER_DATA_PIN 4
#define BUTTON_PIN 2
#define VALVE_PIN 9
#define LED_1_PIN 5
#define LED_2_PIN 6
#define LED_3_PIN 7
#define LED_IRRIGATING_PIN 8

struct EepromAdresses {
  uint8_t hour;
  uint8_t minute;
  uint8_t enable;
  uint8_t duration;
};

static EepromAdresses eepromAdress[3] =  {
  {0x00, 0x01, 0x02, 0x03},
  {0x04, 0x05, 0x06, 0x07},
  {0x08, 0x09, 0x0A, 0x0B}
};

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
RTC_DS1307 rtc;

struct Screen {
  String title;
  uint8_t hour;
  uint8_t minute;
  uint8_t duration;
  uint8_t enable;
};

struct InputFlag {
  uint8_t encoderUp;
  uint8_t encoderDown;
  uint8_t buttonShort;
  uint8_t buttonLong;
  uint8_t timeUpdate;
};

const uint8_t numOfScreens = 5;
uint8_t currentScreen = 0;

Screen screens[numOfScreens] = {
  {"Time:", 4, 20, 0},
  {"Irrigation 1:", 0, 0, 0, 0},
  {"Irrigation 2:", 0, 0, 0, 0},
  {"Irrigation 3:", 0, 0, 0, 0},
  {"Set time:", 0, 0, 0, 0}
};

uint8_t backlightOn = 1;
uint8_t parameterSetting = 0;
DateTime currentTime;
uint8_t lastTimeUpdate;
uint32_t irrigationStartTime;
uint8_t irrigationActive;

InputFlag inputFlags = {0, 0, 0, 0, 0};
// Encoder variables
volatile int16_t encoderPosition = 0;
volatile int16_t lastEncoderPosition = 0;
const uint8_t encoderIncrement = 5;
// Buton variables
const uint16_t buttonShortPress = 100;
const uint16_t buttonLongPress = 750;
volatile uint32_t buttonPressTime;
volatile uint32_t buttonReleaseTime;


void setup() {
  //Serial.begin(9600);
  pinMode(ENCODER_CLK_PIN, INPUT);
  pinMode(ENCODER_DATA_PIN, INPUT);
  pinMode(BUTTON_PIN, INPUT);
  pinMode(VALVE_PIN, OUTPUT);
  pinMode(LED_1_PIN, OUTPUT);
  pinMode(LED_2_PIN, OUTPUT);
  pinMode(LED_3_PIN, OUTPUT);
  pinMode(LED_IRRIGATING_PIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(ENCODER_CLK_PIN), encoderAction, RISING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonAction, CHANGE);
  
  lcd.begin(16,2);
  lcd.backlight();

  cleanEeprom();

  if (! rtc.begin()) {
    lcd.clear();
    lcd.print("RTC error");
    abort();
  }

  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  currentTime = rtc.now();
  screens[0].hour = currentTime.hour();
  screens[0].minute = currentTime.minute();
  lastTimeUpdate = currentTime.minute();

  printScreen();

  for (uint8_t i = 1; i < numOfScreens-1; i++) {
    screens[i].hour = EEPROM.read(eepromAdress[i-1].hour);
    screens[i].minute = EEPROM.read(eepromAdress[i-1].minute);
    screens[i].duration = EEPROM.read(eepromAdress[i-1].duration);
    screens[i].enable = EEPROM.read(eepromAdress[i-1].enable);

  }
}

void loop() {
  setFlags();
  resolveFlags();

  if ((millis() - irrigationStartTime) > (screens[irrigationActive].duration) * 1000) {
    digitalWrite(VALVE_PIN, LOW);
    digitalWrite(LED_IRRIGATING_PIN, LOW);
  }
}

void resolveFlags() {
  if (inputFlags.encoderUp or inputFlags.encoderDown or inputFlags.buttonLong or inputFlags.buttonShort or inputFlags.timeUpdate) {
    switch (parameterSetting) {
      case 0:
        digitalWrite(LED_1_PIN, LOW);
        digitalWrite(LED_2_PIN, LOW);
        digitalWrite(LED_3_PIN, LOW);
        if (inputFlags.encoderUp) {
          if (currentScreen == numOfScreens-1) {
            currentScreen = 0;
          } else {
            currentScreen ++;
          }
        } else if (inputFlags.encoderDown) {
          if (currentScreen == 0) {
            currentScreen = numOfScreens-1;
          } else {
            currentScreen --;
          }
        }
        if (inputFlags.buttonLong) {
          parameterSetting = 1;
        }
        if (inputFlags.buttonShort) {
          if (currentScreen == 0) {
            backlightOn = !backlightOn;
            if (backlightOn) {
              lcd.setBacklight(255);
            } else {
              lcd.setBacklight(0);
            }
          } else {
            screens[currentScreen].enable = !screens[currentScreen].enable;
          }
        }
        break;
        
      case 1:
        digitalWrite(LED_1_PIN, HIGH);
        digitalWrite(LED_2_PIN, LOW);
        digitalWrite(LED_3_PIN, LOW);
        if (inputFlags.encoderUp) {
          if (screens[currentScreen].hour < 23) {
                screens[currentScreen].hour ++;
              }
        } else if (inputFlags.encoderDown) {
          if (screens[currentScreen].hour > 0) {
                screens[currentScreen].hour --;
          }
        }
        if (inputFlags.buttonShort) {
            parameterSetting = 2;
        }
        break;
        
      case 2:
        digitalWrite(LED_1_PIN, LOW);
        digitalWrite(LED_2_PIN, HIGH);
        digitalWrite(LED_3_PIN, LOW);
        if (inputFlags.encoderUp) {
          if (screens[currentScreen].minute < 60) {
                screens[currentScreen].minute ++;
              }
        } else if (inputFlags.encoderDown) {
          if (screens[currentScreen].minute > 0) {
                screens[currentScreen].minute --;
          }
        }
        if (inputFlags.buttonShort) {
            if (currentScreen == numOfScreens-1) {
              rtc.adjust(DateTime(2020, 8, 17, screens[currentScreen].hour, screens[currentScreen].minute, 0));
              parameterSetting = 0;
            } else {
              parameterSetting = 3;
            }
        }
        break;

      case 3:
        digitalWrite(LED_1_PIN, LOW);
        digitalWrite(LED_2_PIN, LOW);
        digitalWrite(LED_3_PIN, HIGH);
        if (inputFlags.encoderUp) {
          if (screens[currentScreen].minute < 60) {
                screens[currentScreen].duration ++;
              }
        } else if (inputFlags.encoderDown) {
          if (screens[currentScreen].minute > 0) {
                screens[currentScreen].duration --;
          }
        }
        if (inputFlags.buttonShort) {
            parameterSetting = 0;
            EEPROM.update(eepromAdress[currentScreen-1].hour, screens[currentScreen].hour);
            EEPROM.update(eepromAdress[currentScreen-1].minute, screens[currentScreen].minute);
            EEPROM.update(eepromAdress[currentScreen-1].enable, screens[currentScreen].enable);
            EEPROM.update(eepromAdress[currentScreen-1].duration, screens[currentScreen].duration);
        }
        break;
    }
    printScreen();
    
    inputFlags.encoderUp = 0;
    inputFlags.encoderDown = 0;
    inputFlags.buttonLong = 0;
    inputFlags.buttonShort = 0;
    inputFlags.timeUpdate = 0;
  }
}

void printScreen() {
  lcd.clear();
  lcd.print(screens[currentScreen].title);
  lcd.setCursor(0,1);
  lcd.print(screens[currentScreen].hour);
  lcd.print(":");
  lcd.print(screens[currentScreen].minute);
  if ((currentScreen != 0) and
      (currentScreen != numOfScreens-1)) {
    lcd.setCursor(7,1);
    lcd.print(screens[currentScreen].duration);
    lcd.print("s");
    if (screens[currentScreen].enable) {
      lcd.setCursor(14,1);
      lcd.print("ON");
    } else {
      lcd.setCursor(13,1);
      lcd.print("OFF");
    }
  }
}

void setFlags() {
  if ((encoderPosition - lastEncoderPosition) > encoderIncrement) {
    inputFlags.encoderUp = 0;
    inputFlags.encoderDown = 1;
    lastEncoderPosition = encoderPosition;
  } else if ((lastEncoderPosition - encoderPosition) > encoderIncrement) {
    inputFlags.encoderUp = 1;
    inputFlags.encoderDown = 0;
    lastEncoderPosition = encoderPosition;
  }
  if (buttonReleaseTime != 0) {
    if (buttonReleaseTime - buttonPressTime > buttonLongPress) {
      inputFlags.buttonLong = 1;
      inputFlags.buttonShort = 0;
    } else if (buttonReleaseTime - buttonPressTime > buttonShortPress) {
      inputFlags.buttonLong = 0;
      inputFlags.buttonShort = 1;
    }
  buttonReleaseTime = 0;
  }

  currentTime = rtc.now();
  if (lastTimeUpdate != currentTime.minute()) {
    screens[0].hour = currentTime.hour();
    screens[0].minute = currentTime.minute();
    lastTimeUpdate = currentTime.minute();
    inputFlags.timeUpdate = 1;
    for (uint8_t i = 1; i < (numOfScreens-1); i ++) {
      if ((currentTime.hour() == screens[i].hour) and
          (currentTime.minute() == screens[i].minute) and 
          screens[i].enable) {
            digitalWrite(VALVE_PIN, HIGH);
            digitalWrite(LED_IRRIGATING_PIN, HIGH);
            irrigationStartTime = millis();
            irrigationActive = i;
      }
    }
  }
}

void cleanEeprom() {
  if (CLEAR_EEPROM) {
    lcd.clear();
    lcd.print("Cleaning");
    lcd.setCursor(0, 1);
    lcd.print("EEPROM");
    for (int i = 0 ; i < EEPROM.length() ; i++) {
      EEPROM.write(i, 0);
    }
    lcd.clear();
    lcd.print("DONE!");
    delay(3000);
  }
}

void encoderAction() {
  if (digitalRead(ENCODER_DATA_PIN)) {
    encoderPosition ++;
  } else {
    encoderPosition --;
  }
}
void buttonAction() {
  if (((millis() - buttonPressTime) > buttonShortPress) or
      ((millis() - buttonReleaseTime) > buttonShortPress)) {
    if (digitalRead(BUTTON_PIN)) {
      buttonPressTime = millis();
    } else {
      buttonReleaseTime = millis();
    }
  }
  
}
