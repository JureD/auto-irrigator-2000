# Auto Irrigator 2000
is compact automatic irrigation system besad around ATmega328 (or Arduino UNO/Nano), DS1307 real time clock and 16x2 LCD.

## Features:
- 3 independently adjustable irrigation times
- self contained unit (you don't need a PC to adjust ittigation  times and durations)
- all settings are stored on EEPROM, so data is preserved in case of power loss

## BOM:
- Arduino UNO or ATmega328
    - 16MHz Crystal **(only needed if using ATmega328)**
    - 2x 22pF ceramic capacitors **(only needed if using ATmega328)**
    - 2x 10uF electrolytic capacitor **(only needed if using ATmega328)**
    - LM340T5 5v linear voltage regulator **(only needed if using ATmega328)**
    - 0.1uF electrolytic capacitor **(only needed if using ATmega328)**
    - 10kOHM resistor **(only needed if using ATmega328)**
- DS1307 RTC module (I used HW-111 module)
- 16x2 LCD
- PFC8574 I2C LCD backpack
- push button
- rotary encoder
- 4x LED
- 4x 220OHM resistor
- 10kOHM resistor
- 1kOHM resistor
- enclosure of your liking
- some wires, connectors and other miscellaneous hardware

# Menu diagram
![Auto Irrigator 2000 - Menu diagram][menu-diagram]


# Wiring
![Auto Irrigator 2000 - Schematics][breadboard]

### PCB
I used ATmega328 instead of arduino on my final board. Here is PCB layout of that board.
![Auto Irrigator 2000 - PCB][pseudo-pcb]

## Pictures of mine
![Auto Irrigator 2000][side]
![Auto Irrigator 2000][front]

[menu-diagram]: images/menu_diagram.png
[breadboard]: images/breadboard.png
[pseudo-pcb]: images/pseudo_pcb.jpg
[side]: images/side.jpg
[front]: images/front.jpg
